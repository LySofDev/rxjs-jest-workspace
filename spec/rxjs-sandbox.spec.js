const { Subject, interval } = require( 'rxjs' );
const { take, map } = require( 'rxjs/operators' );

jest.setTimeout( 10000 );

test( 'data pipeline', done => {
    
    const trigger = new Subject();
    
    const dispatcher = new Subject();
    
    dispatcher.subscribe(
        value => console.log( `Received value ${value}` ),
        error => console.log( `Received error ${error}` ),
        () => done()
    );

    trigger.pipe( map( n => n * 2 ) ).subscribe( dispatcher );

    interval( 1000 ).pipe( take( 5 ) ).subscribe( trigger );

} );