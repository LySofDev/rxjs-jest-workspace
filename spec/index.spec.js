const { helloWorld } = require( '../' );

describe( 'helloWorld', () => {
    it( 'is a function', () => {
        expect( typeof helloWorld ).toEqual( 'function' );
    } );

    it( 'returns a string', () => {
        expect( helloWorld() ).toEqual( 'Hello, World!' );
    } );
} );